// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/BoxComponent.h"
#include "Components/StaticMeshComponent.h"
#include "Runtime/CoreUObject//Public/UObject/ConstructorHelpers.h"
#include "Net/UnrealNetwork.h"
#include "GameFramework/Actor.h"
#include "ReplicatedActor.generated.h"

UCLASS()
class COYGAM312_API AReplicatedActor : public AActor
{
	GENERATED_BODY()
	
public:

	//**Pointer to an actor object for the Owner of this object. 
	UPROPERTY(replicated)
	AReplicatedActor *ReplicatedOwner;

	// Sets default values for this actor's properties
	AReplicatedActor();
	
	//**Overloaded constructor for additional post constructopn properties
	AReplicatedActor(const class FPostContructInitializeProperties &PCIP);

	//**Method to get the replicated props
	void GetLifetimeReplicatedProps(TArray<FLifetimeProperty> &OutLifetimeProps) const;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	
	
};
