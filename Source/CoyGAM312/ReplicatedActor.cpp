// Fill out your copyright notice in the Description page of Project Settings.

#include "ReplicatedActor.h"

AReplicatedActor::AReplicatedActor()
{
	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	

	//**Create a Box component
	UBoxComponent *BoxComp = CreateDefaultSubobject<UBoxComponent>(TEXT("RootComponent"));
	//**Set the Box's size
	BoxComp->InitBoxExtent(FVector(50.0f, 50.0f, 50.0f));

	//**Set the collision profile name
	BoxComp->BodyInstance.SetCollisionProfileName(TEXT("OverlapAllDynamic"));

	//**Set the pawn's root component to the Sphere
	RootComponent = BoxComp;

	//**Create the static mesh component
	UStaticMeshComponent* BoxMeshOne = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Cube"));

	//**Attached the mesh to the sphere component
	BoxMeshOne->SetupAttachment(RootComponent);

	//**Find the 3d model asset for the sphere in the starter content folders
	static ConstructorHelpers::FObjectFinder<UStaticMesh> CubeVisualAsset(TEXT("/Game/StarterContent/Shapes/Shape_Cube.Shape_Cube"));
	//**If the visual asset was successfully found, then set its parameters
	if (CubeVisualAsset.Succeeded())
	{
		//**Set the BoxeMesh to the visual asset we just found
		BoxMeshOne->SetStaticMesh(CubeVisualAsset.Object);

		//**Set the relative location of the boxes
		BoxMeshOne->SetRelativeLocation(FVector(0.0f, 0.0f, -30.0f));

		//**Set the scale size of the sphere
		BoxMeshOne->SetWorldScale3D(FVector(0.8, 0.8f, 0.8f));
	}
}

// Sets default values
AReplicatedActor::AReplicatedActor(const class FPostContructInitializeProperties &PCIP)
{
	//**Set the replication flag to true, so that the server knows to replicate this object
	bReplicates = true;
}

// Called when the game starts or when spawned
void AReplicatedActor::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void AReplicatedActor::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

//**Method to get the replciated props
void AReplicatedActor::GetLifetimeReplicatedProps(TArray<FLifetimeProperty> &OutLifetimeProps) const
{
	//**Set the replication for the Owner variable
	DOREPLIFETIME(AReplicatedActor, ReplicatedOwner);

	//**Set other replication conditions for this object
	DOREPLIFETIME_CONDITION(AReplicatedActor, ReplicatedMovement, COND_SimulatedOnly);
}

